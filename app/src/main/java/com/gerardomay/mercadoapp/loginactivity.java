package com.gerardomay.mercadoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class loginactivity extends AppCompatActivity {
    EditText emailet,passwordet;
    Button btnsign;
    private  static String URL_LOGIN="https://phylacterical-conju.000webhostapp.com/login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginactivity);
        emailet=(TextInputEditText)findViewById(R.id.emailet);
        passwordet=(TextInputEditText)findViewById(R.id.passwordet);
        btnsign=(Button)findViewById(R.id.btnsign);
        btnsign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mail=emailet.getText().toString();
                final String pass=passwordet.getText().toString();
                login(mail,pass);
            }
        });
        SharedPreferences sharedPreferences=getApplicationContext().getSharedPreferences("login_sesion",MODE_PRIVATE);
        boolean status_login=sharedPreferences.getBoolean("status",false);
        if(status_login){
            callHome();
        }
    }
    private void callHome(){
        Intent intent=new Intent(loginactivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
    private void login(final String email,final String password){
        btnsign.setVisibility(View.GONE);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String SUCCES=jsonObject.getString("success");
                            JSONArray jsonArray=jsonObject.getJSONArray("login");
                            if(SUCCES.equals("1")){
                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject object=jsonArray.getJSONObject(i);
                                    SharedPreferences sharedPreferences=getApplicationContext().getSharedPreferences("login_sesion",MODE_PRIVATE);
                                    SharedPreferences.Editor editor=sharedPreferences.edit();
                                    editor.putBoolean("status",true);
                                    editor.putString("name",object.getString("name"));
                                    editor.putString("email",object.getString("email"));
                                    editor.putString("phone",object.getString("phone"));
                                    editor.apply();
                                    callHome();
                                }
                               // Toast.makeText(loginactivity.this,"traidos: "+nombra.toString(),Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(loginactivity.this,"veirifica el response",Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(loginactivity.this,"Error catching: "+e.toString(),Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            btnsign.setVisibility(View.VISIBLE);
                        }
                        // btnsign.setText(nombra);
                        btnsign.setVisibility(View.VISIBLE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                         Toast.makeText(loginactivity.this,"Error response: "+error.toString(),Toast.LENGTH_SHORT).show();
                          btnsign.setVisibility(View.VISIBLE);

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("email",email);
                params.put("password",password);
                return params;
            }
        };
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}
