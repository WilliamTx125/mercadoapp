package com.gerardomay.mercadoapp.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gerardomay.mercadoapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterViewPager extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<LinksStore> linksStoreList;



    @Override
    public int getCount() {
        return linksStoreList.size();
    }

    public AdapterViewPager(Context context, List<LinksStore> linksStoreList) {
        this.context = context;
        this.linksStoreList = linksStoreList;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final LinksStore linksStore=linksStoreList.get(position);
        layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.viewstorepager,container,false);

        final ImageView imgstore=view.findViewById(R.id.imagestore);
        final TextView namestore=view.findViewById(R.id.namestore);
        Picasso.get().load(linksStore.getPhoto()).placeholder(R.mipmap.ic_launcher)
                .fit()
                .centerCrop()
                .into(imgstore);
        container.addView(view,0);
        namestore.setText(linksStore.getName()+linksStore.getPhoto());
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager=(ViewPager) container;
        View view=(View)object;
        viewPager.removeView(view);
    }
}
