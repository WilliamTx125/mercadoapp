package com.gerardomay.mercadoapp.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gerardomay.mercadoapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;


    private List<LinksStore> linksStoreList;
    String URL_STORE="https://phylacterical-conju.000webhostapp.com/getStore.php";
    Context mcontext;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        mcontext=container.getContext();
        linksStoreList=new ArrayList<>();
        getStore(root);
        return root;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mcontext=context;
    }

    private void getStore(final View root){
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URL_STORE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                           // String SUCCESS=jsonObject.getString("success");
                            JSONArray jsonArray=jsonObject.getJSONArray("result");
                           // Toast.makeText(getContext(),"response: "+response.toString(),Toast.LENGTH_SHORT).show();
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject object=jsonArray.getJSONObject(i);
                                linksStoreList.add(new LinksStore(
                                        object.getString("name"),
                                        object.getString("photo"),
                                        object.getString("address")
                                ));
                            }
                            Toast.makeText(getContext(),"tamaño array:"+linksStoreList.size(),Toast.LENGTH_SHORT).show();
                            final AdapterViewPager adapter =new AdapterViewPager(mcontext,linksStoreList);
                            CircleIndicator indicator=(CircleIndicator)root.findViewById(R.id.indicator);

                            ViewPager viewPager=(ViewPager)root.findViewById(R.id.viewpager);
                            viewPager.setAdapter(adapter);
                            indicator.setViewPager(viewPager);
                           // ShadowTransformer fragmentCardShadowTransformer=new ShadowTransformer(viewPager,adapter);
                            float density=getResources().getDisplayMetrics().density;
                            float partialwidth=16*density;
                            float pagemargin=8*density;
                            //float viewpagerpadding=
                            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                @Override
                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                    if(position<(adapter.getCount()-1)){

                                    }
                                }

                                @Override
                                public void onPageSelected(int position) {

                                }

                                @Override
                                public void onPageScrollStateChanged(int state) {

                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(mcontext,"Error catching: "+e.toString(),Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mcontext,"Error on response: "+error.toString(),Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parmas=new HashMap<>();
                return parmas;
            }
        };
        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }
}
