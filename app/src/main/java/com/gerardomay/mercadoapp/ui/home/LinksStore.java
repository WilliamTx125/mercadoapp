package com.gerardomay.mercadoapp.ui.home;

public class LinksStore {
    private String name, photo, address;

    public LinksStore(String name, String photo, String address) {
        this.name = name;
        this.photo = photo;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
