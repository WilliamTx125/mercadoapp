package com.gerardomay.mercadoapp.ui.home;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

public class ShadowTransformer implements ViewPager.OnPageChangeListener, ViewPager.PageTransformer {
    private ViewPager viewPager;
    private CardAdapter cardAdapter;
    private float lastOffset;
    private boolean scalingEnabled;

    public ShadowTransformer(ViewPager viewPager, CardAdapter cardAdapter) {
        this.viewPager = viewPager;
        viewPager.addOnPageChangeListener(this);
        this.cardAdapter = cardAdapter;
    }
    public void enableScaling(boolean enable){
        if (scalingEnabled && !enable){
            CardView currentCard=cardAdapter.getCardViewAt(viewPager.getCurrentItem());
            if (currentCard !=null ){
                currentCard.animate().scaleY(1);
                currentCard.animate().scaleX(1);
            }
        } else if(!scalingEnabled && enable){
            CardView currentCard=cardAdapter.getCardViewAt(viewPager.getCurrentItem());
            if (currentCard != null){
                currentCard.animate().scaleY(1.1f);
                currentCard.animate().scaleX(1.1f);
            }
        }
        scalingEnabled=enable;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        int realCurrentPosition;
        int nextPosition;
        float baseElevation=cardAdapter.getBaseElevation();
        float realOffset;
        boolean goingLeft=lastOffset>positionOffset;
        // Si vamos hacia atrás, onPageScrolled recibe la última posición
        // en lugar del actual
        if(goingLeft){
            realCurrentPosition=position+1;
            nextPosition=position;
            realOffset=1-positionOffset;
        } else{
            nextPosition=position+1;
            realCurrentPosition=position;
            realOffset=positionOffset;
        }
        // Evita choques en el desplazamiento excesivo
        if(nextPosition>cardAdapter.getCount()-1 || realCurrentPosition>cardAdapter.getCount()-1){
            return;
        }

        CardView currentCard=cardAdapter.getCardViewAt(realCurrentPosition);
        // Esto podría ser nulo si se está utilizando un fragmento
        // y las vistas aún no se crearon
        if (currentCard !=null){
            if(scalingEnabled){
                currentCard.setScaleX((float)(1+0.1*(1-realOffset)));
                currentCard.setScaleY((float) (1+0.1*(1-realOffset)));
            }
            currentCard.setCardElevation(baseElevation + baseElevation *
                    (CardAdapter.MAX_ELEVATION_FACTOR - 1)*(1 - realOffset));
        }
        CardView nextCard=cardAdapter.getCardViewAt(nextPosition);
        // Es posible que estemos desplazándonos lo suficientemente rápido para que la siguiente (o anterior) tarjeta
        // ya se destruyó o es posible que aún no se haya creado un fragmento
        if (nextCard != null){
            if (scalingEnabled){
                nextCard.setScaleX((float) (1 + 0.1 * (realOffset)));
                nextCard.setScaleY((float) (1 + 0.1 * (realOffset)));
            }
            nextCard.setCardElevation((baseElevation + baseElevation *
                    (CardAdapter.MAX_ELEVATION_FACTOR - 1)*(realOffset)));
        }
        lastOffset=positionOffset;
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void transformPage(@NonNull View page, float position) {

    }
}
